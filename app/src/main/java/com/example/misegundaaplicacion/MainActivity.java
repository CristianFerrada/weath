package com.example.misegundaaplicacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.misegundaaplicacion.R;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {




    private TextView tChillan;
    private TextView hChillan;
    private TextView pChillan;
    private TextView tMoscu;
    private TextView hMoscu;
    private TextView pMoscu;
    private TextView tParis;
    private TextView hParis;
    private TextView pParis;
    private TextView tMadrid;
    private TextView hMadrid;
    private TextView pMadrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //chillan
        this.tChillan = (TextView) findViewById(R.id.tempChillan);
        this.hChillan = (TextView) findViewById(R.id.humChillan);
        this.pChillan = (TextView) findViewById(R.id.preChillan);
        //moscu
        this.tMoscu = (TextView) findViewById(R.id.tempMoscu);
        this.hMoscu = (TextView) findViewById(R.id.humMoscu);
        this.pMoscu = (TextView) findViewById(R.id.preMoscu);
        //paris
        this.tParis= (TextView) findViewById(R.id.tempParis);
        this.hParis = (TextView) findViewById(R.id.humParis);
        this.pParis = (TextView) findViewById(R.id.preParis);
        //madrid
        this.tMadrid = (TextView) findViewById(R.id.tempMadrid);
        this.hMadrid = (TextView) findViewById(R.id.humMadrid);
        this.pMadrid= (TextView) findViewById(R.id.preMadrid);

//asdsad


        String urlChillan = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6067&lon=-72.1034&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        String urlMoscu = "http://api.openweathermap.org/data/2.5/weather?lat=55.748723&lon=37.624803&appid=980371df6c9e63b6d8bcdb27439132b3&units=metric";
        String urlParis = "http://api.openweathermap.org/data/2.5/weather?lat=48.8612147&lon=2.338978&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";
        String urlMadrid = "http://api.openweathermap.org/data/2.5/weather?lat=40.4167421&lon=-3.7045128&appid=4ee33b0d0eb874be41098193ffc25e84&units=metric";

        StringRequest solicitud1 = new StringRequest(
                Request.Method.GET,
                urlChillan,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            //JSON
                            JSONObject tChiJSON = respuestaJSON.getJSONObject("main");
                            double tChi = tChiJSON.getDouble("temp");

                            JSONObject hChiJSON = respuestaJSON.getJSONObject("main");
                            double hChi = hChiJSON.getDouble("humidity");

                            JSONObject pChiJSON = respuestaJSON.getJSONObject("main");
                            double pChi = pChiJSON.getDouble("pressure");

                            //toast
                            Toast.makeText(getApplicationContext(), "" + tChi + "", Toast.LENGTH_SHORT).show();
                            tChillan.setText("" + tChi + "");

                            Toast.makeText(getApplicationContext(), "" + hChi + "", Toast.LENGTH_SHORT).show();
                            hChillan.setText("" + tChi + "");

                            Toast.makeText(getApplicationContext(), "" + pChi + "", Toast.LENGTH_SHORT).show();
                            pChillan.setText("" + tChi + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        StringRequest solicitud2 = new StringRequest(
                Request.Method.GET,
                urlMoscu,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            //JSON
                            JSONObject tMosJSON = respuestaJSON.getJSONObject("main");
                            double tMos = tMosJSON.getDouble("temp");

                            JSONObject hMosJSON = respuestaJSON.getJSONObject("main");
                            double hMos = hMosJSON.getDouble("humidity");

                            JSONObject pMosJSON = respuestaJSON.getJSONObject("main");
                            double pMos = pMosJSON.getDouble("pressure");

                            //toast
                            Toast.makeText(getApplicationContext(), "" + tMos + "", Toast.LENGTH_SHORT).show();
                            tMoscu.setText("" + tMos + "");

                            Toast.makeText(getApplicationContext(), "" + hMos + "", Toast.LENGTH_SHORT).show();
                            hMoscu.setText("" + hMos + "");

                            Toast.makeText(getApplicationContext(), "" + pMos + "", Toast.LENGTH_SHORT).show();
                            pMoscu.setText("" + pMos + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );StringRequest solicitud3 = new StringRequest(
                Request.Method.GET,
                urlParis,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            //JSON
                            JSONObject tParJSON = respuestaJSON.getJSONObject("main");
                            double tPar = tParJSON.getDouble("temp");

                            JSONObject hParJSON = respuestaJSON.getJSONObject("main");
                            double hPar = hParJSON.getDouble("humidity");

                            JSONObject pParJSON = respuestaJSON.getJSONObject("main");
                            double pPar = pParJSON.getDouble("pressure");

                            //toast
                            Toast.makeText(getApplicationContext(), "" + tPar + "", Toast.LENGTH_SHORT).show();
                            tChillan.setText("" + tPar + "");

                            Toast.makeText(getApplicationContext(), "" + hPar + "", Toast.LENGTH_SHORT).show();
                            hChillan.setText("" + hPar + "");

                            Toast.makeText(getApplicationContext(), "" + pPar + "", Toast.LENGTH_SHORT).show();
                            pChillan.setText("" + pPar + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );StringRequest solicitud4 = new StringRequest(
                Request.Method.GET,
                urlMadrid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            //JSON
                            JSONObject tMadJSON = respuestaJSON.getJSONObject("main");
                            double tMad = tMadJSON.getDouble("temp");

                            JSONObject hMadJSON = respuestaJSON.getJSONObject("main");
                            double hMad = hMadJSON.getDouble("humidity");

                            JSONObject pMadJSON = respuestaJSON.getJSONObject("main");
                            double pMad = pMadJSON.getDouble("pressure");

                            //toast
                            Toast.makeText(getApplicationContext(), "" + tMad + "", Toast.LENGTH_SHORT).show();
                            tChillan.setText("" + tMad  + "");

                            Toast.makeText(getApplicationContext(), "" + hMad + "", Toast.LENGTH_SHORT).show();
                            hChillan.setText("" + hMad + "");

                            Toast.makeText(getApplicationContext(), "" + pMad + "", Toast.LENGTH_SHORT).show();
                            pChillan.setText("" + pMad + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud1);
        listaEspera.add(solicitud2);
        listaEspera.add(solicitud3);
        listaEspera.add(solicitud4);

    }
}
